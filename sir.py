# Name your solution file sir.py
#
import numpy as np 
#from scikits import bvp_solver 
import matplotlib.pyplot as plt 
from scipy.integrate import odeint

from pybvp6c.bvp6c import bvp6c, bvpinit, deval
from pybvp6c.structure_variable import struct



def prob1(n=100):
	# n is the number of subintervals in the interval [0,100] 
	
	# x = np.linspace(0,100,n+1) 
	# S, I, R = the solutions at the grid points in x, in one dimensional ndarrays. 
	
	t=np.linspace(0,100,n+1)

	def ODE(y,x):
		return(-.5*y[0]*y[1],.5*y[0]*y[1]-.25*y[1],.25*y[1])
	
	#problem=bvp_solver.ProblemDefinition(num_ODE=3,num_parameters=0,num_left_boundary_conditions=1,boundary_points=(1-6.25e-7,6.25e-7,0),function=ode)	

	y=odeint(ODE,np.array([1-6.25e-7,6.25e-7,0]),t)
	'''
	plt.plot(t, y[:,0],'-k',linewidth=2.0)
	plt.plot(t, y[:,1],'-r',linewidth=2.0)
	plt.plot(t, y[:,2],'-b',linewidth=2.0)

	plt.show()	
	'''
	return t, y[:,0], y[:,1], y[:,2]



def prob2():
	# Plot and show the solution: S, I, and R, on the interval [0,50]. 
	# Answer the two questions asked in the lab, and return them in 
	# variables three and seven. 

	days=3.
	contact=1.
	prop=5./3000000.

	def ODE1(y,x):
		return(-contact*y[0]*y[1],contact*y[0]*y[1]-(1./days)*y[1],(1./days)*y[1])

	t=np.linspace(0,50,51)

	y=odeint(ODE1,np.array([1-prop,prop,0]),t)
	
	
	plt.plot(t, y[:,0],'-k',linewidth=2.0)
	plt.plot(t, y[:,1],'-r',linewidth=2.0)
	plt.plot(t, y[:,2],'-b',linewidth=2.0)
	
	plt.show()
	
	days2=7.
	
	def ODE2(y,x):
		return(-contact*y[0]*y[1],contact*y[0]*y[1]-(1./days2)*y[1],(1./days2)*y[1])

	y2=odeint(ODE2,np.array([1-prop,prop,0]),t)
	
	plt.plot(t, y2[:,0],'-k',linewidth=2.0)
	plt.plot(t, y2[:,1],'-r',linewidth=2.0)
	plt.plot(t, y2[:,2],'-b',linewidth=2.0)

	plt.show()
	
	three=1-max(y[:,1])
	
	seven=1-max(y2[:,1])
	
	return int(three*3000000), int(seven*3000000)


def prob3():
	# Plot and show your solution. 
	
	days=4.
	contact=3./10.
	prop=5./3000000.

	def ODE1(y,x):
		return(-contact*y[0]*y[1],contact*y[0]*y[1]-(1./days)*y[1],(1./days)*y[1])

	t=np.linspace(0,500,501)

	y=odeint(ODE1,np.array([1-prop,prop,0]),t)

	plt.plot(t, y[:,0],'-k',linewidth=2.0)
	plt.plot(t, y[:,1],'-r',linewidth=2.0)
	plt.plot(t, y[:,2],'-b',linewidth=2.0)
	
	plt.show()


def prob4():
	# Plot and show your solution. 
	
	# Return the constants C mentioned in the problem. Return it in a 
	# one-dimensional array of length 3. 

	b1=1
	b0=1575
	per=.01
	lam=.0279
	mu=.02

	options=struct()

	def f_jacobian(x,y):
		return np.array([[b0*(1.+b1*np.cos(2*np.pi*x))*y[2],b0*(1.+b1*np.cos(2*np.pi*x))*y[2],0,0,0,0],[0,-1./lam,1./lam,0,0,0],[b0*(1+b1*np.cos(2*np.pi*x))*y[2],b0*(1+b1*np.cos(2*np.pi*x))*y[2],-1./per,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0]])

	def bc_jacobian(x,y):
		ya=np.array([[1.,0,0,-1.,0,0],[0,1.,0,0,-1.,0],[0,0,1.,0,0,-1.],[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0]])
		yb=np.array([[0,0,0,0,0,0],[0,0,0,0,0,0],[0,0,0,0,0,0],[1.,0,0,-1.,0,0],[0,1.,0,0,-1.,0],[0,0,1.,0,0,-1.]])
		return ya,yb


	def ode(x,y):
		return np.array([mu-b0*(1+b1*np.cos(2*np.pi*x))*y[0]*y[2],b0*(1+b1*np.cos(2*np.pi*x))*y[0]*y[2]-(y[1]/lam),(y[1]/lam)-y[2]/per,0,0,0])

	def bcs(ya,yb):
		return np.array([ya[0]-ya[3],ya[1]-ya[4],ya[2]-ya[5],yb[0]-yb[3],yb[1]-yb[4],yb[2]-yb[5]])

	def init(x):
		S = .1 + .05 * np.cos(2. * np.pi * x)
		return np.array([.65 + .4*S, .05 * (1. - S)-.042, .05 * (1. - S)-.042, .075,.005,.005])
	
	options=struct()
	options.fjacobian, options.bcjacobian = f_jacobian, bc_jacobian

	solinit = bvpinit(np.linspace(0,1,101),init)
	sol = bvp6c(ode,bcs,solinit,options)
	
	t=np.linspace(0,500,501)

	plt.plot(t, sol[:,0],'-k',linewidth=2.0)
	plt.plot(t, sol[:,1],'-r',linewidth=2.0)
	plt.plot(t, sol[:,2],'-b',linewidth=2.0)
	
	plt.show()


