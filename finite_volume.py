import numpy as np
from matplotlib import pyplot as plt
from math import floor

def upwind(u0,a,xmin,xmax,t_final,nt):
    dt = float(t_final) / nt
    # Since we are doing periodic boundary conditions,
    # we need to divide by u0.size instead of (u0.size - 1).
    dx = float(xmax - xmin) / u0.size
    lambda_ = a * dt / dx
    u = u0.copy()
    for j in xrange(nt):
        # The Upwind method. The np.roll function helps us
        # account for the periodic boundary conditions.
        u -= lambda_ * (u - np.roll(u, 1))
    # Get the x values for the plots.
    x = np.linspace(xmin, xmax, u0.size+1)[:-1]
    # Plot the computed solution.
    plt.plot(x, u, label='Upwind Method')
    # Find the exact solution and plot it.
    distance = a * t_final
    roll = int((distance - floor(distance)) * u0.size)
    plt.plot(x, np.roll(u0, roll), label='Exact solution')
    # Show the plot with the legend.
    plt.legend(loc='best')
    plt.show()



def lax_wendroff(u0, a, xmin, xmax, t_final, nt):
    """ Solve the advection equation with periodic
    boundary conditions on the interval [xmin, xmax]
    using the Lax-Wendroff finite volume scheme.
    Use u0 as the initial conditions.
    a is the constant from the PDE.
    Use the size of u0 as the number of nodes in
    the spatial dimension.
    Let nt be the number of spaces in the time dimension
    (this is the same as the number of steps if you do
    not include the initial state).
    Plot and show the computed solution along
    with the exact solution. """
    dt = float(t_final) / nt
    dx = float(xmax - xmin) / u0.size
    lambda_ = a * dt / dx
    u = u0.copy()
    m = np.zeros(u0.shape)
    
    for j in xrange(nt):
        m=(np.roll(u,-1)-u)/dx
        u -= lambda_ * (u-np.roll(u,1)+m*.5*(dx-a*dt)-.5*np.roll(m,1)*(dx-a*dt))

    x = np.linspace(xmin, xmax, u0.size+1)[:-1]
    plt.plot(x, u, label='Lax-Wendroff Method')
    distance = a * t_final
    roll = int((distance - floor(distance)) * u0.size)
    plt.plot(x, np.roll(u0, roll), label='Exact solution')

    plt.legend(loc='best')
    plt.show()



def minimod(u0, a, xmin, xmax, t_final, nt):
    """ Solve the advection equation with periodic
    boundary conditions on the interval [xmin, xmax]
    using the minimod finite volume scheme.
    Use u0 as the initial conditions.
    a is the constant from the PDE.
    Use the size of u0 as the number of nodes in
    the spatial dimension.
    Let nt be the number of spaces in the time dimension
    (this is the same as the number of steps if you do
    not include the initial state).
    Plot and show the computed solution along
    with the exact solution. """
    def minmod(a,b):
        x=np.abs(a)<=np.abs(b)
        y=np.abs(b)<np.abs(a)
        z=a*b>np.zeros(a.shape)


        return (x*a+y*b)*z

    dt = float(t_final) / nt
    dx = float(xmax - xmin) / u0.size
    lambda_ = a * dt / dx
    u = u0.copy()
    m = u0.copy()
    for j in xrange(nt):
        m=minmod((u-np.roll(u,1))/dx,(np.roll(u,-1)-u)/dx)
        u -= lambda_ * (u-np.roll(u,1)+m*.5*(dx-a*dt)-.5*np.roll(m,1)*(dx-a*dt))

    x = np.linspace(xmin, xmax, u0.size+1)[:-1]

    plt.plot(x, u, label='MinMod Method')
    distance = a * t_final
    roll = int((distance - floor(distance)) * u0.size)
    plt.plot(x, np.roll(u0, roll), label='Exact solution')

    plt.legend(loc='best')
    plt.show()


nx = 100
nt = nx * 3 // 2
x = np.linspace(0., 1., nx+1)[:-1]
u0 = np.exp(-(x - .3)**2 / .005)
arr = (.6 < x) & (x < .7 )
u0[arr] += 1.
# Run the simulation.
minimod(u0, 1.2, 0, 1, 1.2, nt)
