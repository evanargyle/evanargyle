import numpy as np
from matplotlib import pyplot as plt

#########Prob 1##############
def prob1():
	return 'HIV is a non-living virus that replicates at the expense of T cells. AIDS is the loss of T cells. This compromises the human immune system, which can lead to death by infection. '

#########Prob 2##############
def prob2():
	return 'AIDS can be treated using drugs that boost the immune system and increasing T cell count.'



#Code from RK4 Lab with minor edits
def initialize_all(y0, t0, t, n):
	""" An initialization routine for the different ODE solving methods in the lab. This initializes Y, T, and h. """
	if isinstance(y0, np.ndarray):
		Y = np.empty((n, y0.size)).squeeze()
	else:
		Y = np.empty(n)
	Y[0] = y0
	T = np.linspace(t0, t, n)
	h = float(t - t0) / (n - 1)
	return Y, T, h

def RK4(f, y0, t0, t, n):
	""" Use the RK4 method to compute an approximate solution to the ODE y' = f(t, y) at n equispaced parameter values from t0 to t with initial conditions y(t0) = y0. y0 is assumed to be either a constant or a one-dimensional numpy array. t and t0 are assumed to be constants.	f is assumed to accept three arguments.	The first is a constant giving the value of t. The second is a one-dimensional numpy array of the same size as y. The third is an index to the other arrays. This function returns an array Y of shape (n,) if y is a constant or an array of size 1. It returns an array of shape (n, y.size) otherwise. In either case, Y[i] is the approximate value of y at the i'th value of np.linspace(t0, t, n). """
	Y,T,h = initialize_all(y0,t0,t,n)
	for i in xrange(n-1):
		K1 = f(T[i],Y[i],i)
		K2 = f(T[i]+h/2.,Y[i]+h/2.*K1,i)
		K3 = f(T[i]+h/2.,Y[i]+h/2.*K2,i)
		K4 = f(T[i+1],Y[i]+h*K3,i)
		Y[i+1] = Y[i] + h/6.*(K1+2*K2 +2*K3+K4)
	return Y


def prob5():
	a_1, a_2 = 0, 0
	b_1, b_2 = 0.02, 0.9
	s_1, s_2 = 2., 1.5
	mu = 0.002
	k = 0.000025
	g = 30.
	c = 0.007
	B_1, B_2 = 14, 1
	A_1, A_2 = 250000, 75
	T_0, V_0 = 400, 3
	t_f = 50
	n = 1000

	state = np.ones((n,2))
	state0 = np.array([T_0, V_0])
	costate = np.zeros((n,2))
	costate0 = np.zeros(2)
	u=np.zeros((n,2))
	u[:,0] += .02
	u[:,1] += .9

	def state_equations(t,y,i):
		T=y[0]
		V=y[1]
	
		return np.array([s_1-s_2*V/(B_1+V)-mu*T-k*V*T+u[i,0]*T,(1-u[i,1])*g*V/(B_2+V)-c*V*T])

	def lambda_hat(t,y,i):
		i=-1-i
		l1=y[0]
		l2=y[1]

		L1=l1*(-mu-k*state[i,1]+u[i,0])-c*l2*state[i,1]+1.

		L2=l2*((g*B_2*(1-u[i,1])/((B_2+state[i,1])**2))-c*state[i,0])-l1*(k*state[i,0]+(s_2*B_1/(B_1+state[i,1])**2))

		return np.array([L1,L2])



	epsilon = 0.001
	test = epsilon + 1
	count=0
	while(test > epsilon):
		count+=1
		oldu = u.copy();
		#solve the state equations with forward iteration
		state = RK4(state_equations,np.array([T_0,V_0]),0,t_f,n)
		#solve the costate equations with backwards iteration
		costate = RK4(lambda_hat,costate0,0,t_f,n)[::-1]
		#solve for u1 and u2
		u1=np.minimum(np.maximum([a_1]*n,(1./(2.*A_1))*costate[:,0]*state[:,0]),[b_1]*n)
		u2=np.minimum(np.maximum([a_2]*n,-(costate[:,1]/(2.*A_2))*g*state[:,1]/(B_2+state[:,1])),[b_2]*n)
		#update control
		u[:,0] = 0.5*(u1 + oldu[:,0])
		u[:,1] = 0.5*(u2 + oldu[:,1])
		#test for convergence
		test = abs(oldu - u).sum()
	
	plt.subplot(2,2,1)
	plt.plot(np.linspace(0,50,n),state[:,0])
	plt.title("T")
	plt.subplot(2,2,2)
	plt.plot(np.linspace(0,50,n),state[:,1])
	plt.title("V")
	plt.subplot(2,2,3)
	plt.plot(np.linspace(0,50,n),u[:,0])
	plt.title("u1")
	plt.subplot(2,2,4)
	plt.plot(np.linspace(0,50,n),u[:,1])
	plt.title("u2")
	plt.show()

#prob5()
