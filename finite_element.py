# Call this lab finite_element.py

import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg import solve
from scipy.linalg import solve_banded
from scipy import sparse
from numpy.linalg import norm


def prob1():
	"""
	Plot the true solution, along with your numerical solution.
	"""
	alpha=2
	beta=4
	eps=.02

	X=np.linspace(0,1,101)

	h=1./101.

	A=np.diag([-2*eps/h]*101)+np.diag([.5+eps/h]*99+[0],k=-1)+np.diag([0]+[-.5+eps/h]*99,k=1)
	
	A[0][0],A[-1][-1]=1,1
	
	omega=np.array([alpha]+[-1*h]*99+[beta])
	
	K=solve(A,omega)
	print K
	plt.plot(X,[2+x+(np.exp(x/.02)-1)/(np.exp(1/.02)-1) for x in X])
	plt.plot(X,K)
	plt.show()


def prob2():
	""" 
	Plot the true solution, and your numerical solutions. Reproduce the 
	graph shown in the lab.  
	
	"""
	alpha=2
	beta=4
	eps=.02

	even_grid=np.linspace(0,1,15)
	clustered_grid=np.linspace(0,1,15)**(1./8.)

	h1=[even_grid[x]-even_grid[x-1] for x in xrange(1,15)]
	h2=[clustered_grid[x]-clustered_grid[x-1] for x in xrange(1,15)]

	A1=np.diag([1]+[-1*(eps/h1[x]+eps/h1[x+1]) for x in xrange(13)]+[1])+np.diag([.5+eps/h1[x] for x in xrange(13)]+[0],k=-1)+np.diag([0]+[-.5+eps/h1[x] for x in xrange(13)],k=1)
	A2=np.diag([1]+[-1*(eps/h2[x]+eps/h2[x+1]) for x in xrange(13)]+[1])+np.diag([.5+eps/h2[x] for x in xrange(13)]+[0],k=-1)+np.diag([0]+[-.5+eps/h2[x+1] for x in xrange(13)],k=1)

	omega1=[alpha]+[-.5*(h1[x]+h1[x+1]) for x in xrange(13)]+[beta]
	omega2=[alpha]+[-.5*(h2[x]+h2[x+1]) for x in xrange(13)]+[beta]
	
	K1=solve(A1,omega1)
	K2=solve(A2,omega2)
	
	plt.plot(even_grid,K1,"-o",label="Even")
	plt.plot(clustered_grid,K2,"-o",label="Clustered")
	X=np.linspace(0,1,101)
	plt.plot(X,[2+x+(np.exp(x/.02)-1)/(np.exp(1/.02)-1) for x in X],label="Solution")
	plt.legend()
	plt.show()



def prob3():
	""" 
	Plot and show a log-log graph, showing both the error in the 
	second order finite element approximations, and the error in the 
	pseudospectral approximations. 
	
	You can verify the error for the second order finite element 	
	approximations using the graph shown in the lab. 
	
	"""
	points=[]
	errors=[]

	for i in xrange(4,23):
		alpha=2
		beta=4
		eps=.02

		n=2**i
		X=np.linspace(0,1,n+1)

		h=1./(n+1)

		A=np.array([[0]+[0]+[-.5+eps/h]*(n-1),[1]+[-2*eps/h]*(n-1)+[1],[.5+eps/h]*(n-1)+[0]+[0]])
	
		omega=np.array([alpha]+[-1*h]*(n-1)+[beta])

		sol=solve_banded((1,1), A, omega)

		error=max(abs((sol-[alpha+x+(beta-alpha-1)*(np.exp(x/eps)-1)/(np.exp(1/eps)-1) for x in X])))

		points.append(n)
		errors.append(error)

	pseudo_error=[]
	pseudo_points=[]

	for i in xrange(4,11):
		alpha=2
		beta=4
		eps=.02

		n=2**i
		def cheb(N):
			x=np.cos((np.pi/N)*np.linspace(0,N,N+1))
			x.shape=(N+1,1)
			lin=np.linspace(0,N,N+1)
			lin.shape = (N+1,1)
			c=np.ones((N+1,1))
			c[0],c[-1]=2.,2.
			c=c*(-1.)**lin
			X=x*np.ones(N+1)

			dX=X-X.T
			D=(c*(1./c).T)/(dX + np.eye(N+1))
			D = D - np.diag(np.sum(D.T,axis=0))
			x.shape = (N+1,)
			return D, x
		XX=np.linspace(0,1,n+1)
		D,x=cheb(n)
		D2=2*(D.dot(D)*eps-D)
		D2[0],D2[-1]=np.zeros(n+1),np.zeros(n+1)
		D2[0,0],D2[-1,-1]=1.,1.

		x=(x+1)*.5
		f=[-1 for g in x]
		f[0],f[-1]=beta,alpha
		U=solve(D2,f)
		
		pseudo_error.append(abs(max(U-[((alpha+g+(beta-alpha-1)*(np.exp(g/eps)-1)/(np.exp(1/eps)-1))) for g in x])))
		pseudo_points.append(n)
		
	plt.loglog(points,errors)
	plt.loglog(pseudo_points,pseudo_error)
	plt.show()
	
	

prob3()
