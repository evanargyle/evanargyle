from math import log
import numpy as np
from matplotlib import pyplot as plt
from scipy.integrate import ode
from scipy.integrate import odeint

rho_F = 9400.
rho_L = 1800.
gamma_F = 3.2
gamma_L = 22.
eta_F = 180.
eta_L = 230.
C=10.4 #Forbes constant
beta_AT = 0.14 #Adaptive Thermogenesis
beta_TEF = 0.1 #Thermic Effect of Feeding
K=0

def forbes(F):
	C1 = C * rho_L / rho_F
	return C1 / (C1 + F)

def energy_balance(F, L, EI, PAL):
	p=forbes(F)
	a1 = (1. / PAL - beta_AT) * EI - K - gamma_F * F - gamma_L * L
	a2 = (1 - p) * eta_F / rho_F + p * eta_L / rho_L + 1. / PAL
	return a1 / a2

def weight_odesystem(t, y, EI, PAL):
	F, L = y[0], y[1]
	p, EB = forbes(F), energy_balance(F, L, EI, PAL)
	return np.array([(1 - p) * EB / rho_F , p * EB / rho_L])

def fat_mass(BW, age, H, sex):
	BMI = BW / H**2.
	if sex =='male':
		return BW * (-103.91 + 37.31 * log(BMI) + 0.14 * age) / 100
	else:
		return BW * (-102.01 + 39.96 * log(BMI) + 0.14 * age) / 100

def RK4(f, y0, t0, t, n):
	y0,t0,h=initialize_all(y0, t0, t, n)
	for x in xrange(len(y0)-1):
		K1=f(t0[x],y0[x])
		K2=f(t0[x]+h/2,y0[x]+(h/2)*K1)
		K3=f(t0[x]+h/2,y0[x]+(h/2)*K2)
		K4=f(t0[x+1],y0[x]+h*K3)
		y0[x+1]=y0[x]+(h/6)*(K1+K2*2+K3*2+K4)
	return y0,t0

def prob1():
	""" Using the RK4 method, approximate the solution curve for a 
		single-stage weightloss intervention. 
	Plot and show your solution. 
	"""
	BW=72.5748
	F0=fat_mass(BW,38,1.7272,'female')
	L0=BW-F0
	def ODE(t,y):
		return weight_odesystem(t,y, 2025, 1.5)
	t=np.linspace(0,365*5,365*5)
	Y0=[F0,L0]
	y=np.empty((len(t),len(Y0)))
	y[0]=Y0
	r=ode(ODE).set_integrator('dopri5')
	r.set_initial_value(Y0,t[0])
	while r.successful() and r.t<t[-2]:
		y[r.t]=r.integrate(r.t+1)
	
	plt.plot(t,y[:,0],label='Fat')
	plt.plot(t,y[:,1],label='Lean')
	plt.plot(t,y[:,0]+y[:,1],label='Total')
	plt.legend()
	plt.show()
	
prob1()

def prob2():
	""" Using the RK4 method, approximate the solution curve for a 
	two-stage weightloss intervention. 
    Plot and show your solution. 
	"""
	BW=72.5748
	F0=fat_mass(BW,38,1.7272,'female')
	L0=BW-F0
	def ODE(t,y):
		return weight_odesystem(t,y, 1600, 1.7)
	def ODE2(t,y):
		return weight_odesystem(t,y, 2025, 1.5)
	t=np.linspace(0,224,224)
	Y0=[F0,L0]
	y=np.empty((len(t),len(Y0)))
	y[0]=Y0
	r=ode(ODE).set_integrator('dopri5')
	r.set_initial_value(Y0,0)
	while r.successful() and r.t<112:
		y[r.t]=r.integrate(r.t+1)
	Y1=y[112]
	r2=ode(ODE2).set_integrator('dopri5')	
	r2.set_initial_value(Y1,112)
	while r2.successful() and r2.t<223:
		y[r2.t]=r2.integrate(r2.t+1)

	plt.plot(t,y[:,0],label='Fat')
	plt.plot(t,y[:,1],label='Lean')
	plt.plot(t,y[:,0]+y[:,1],label='Total')
	plt.legend()
	plt.show()



def prob3():
	""" Using the RK4 method, approximate the predator-prey trajectories.
	Plot and show the solutions. 
	"""
	a, b = 0., 13
	alpha = 1. / 3
	dim = 2
	y0 = np.array([1 / 2., 1 / 3.])
	def Lotka_Volterra(y, x):
		return np.array([y[0] * (1. - y[1]), alpha * y[1] * (y[0] - 1.)])
	subintervals=200
	Y=odeint(Lotka_Volterra,y0,np.linspace(a,b,subintervals))
	Y1, Y2 = np.meshgrid(np.arange(0, 4.5, .2), np.arange(0, 4.5, .2), sparse=True, copy=False)
	U, V = Lotka_Volterra((Y1, Y2), 0)
	Q=plt.quiver(Y1[::3,::3],Y2[::3,::3], U[::3,::3], V[::3,::3],pivot='mid',color = 'b',units='dots',width=3.)
	plt.plot(1, 1,'ok',markersize=8)
	plt.plot(0, 0,'ok',markersize=8)
	plt.plot(Y[:,0], Y[:,1],'-k',linewidth=2.0)
	plt.plot(Y[::10,0], Y[::10,1],'*b')
	plt.axis([-.5, 4.5, -.5, 4.5])
	plt.title(" Phase Portrait of the Lotka - Volterra Predator - Prey Model")
	plt.xlabel('Prey',fontsize=15)
	plt.ylabel('Predators',fontsize=15)

	#For U,V=(.5,.75)
	Y=odeint(Lotka_Volterra,np.array([.5,.75]),np.linspace(a,b,subintervals))
	plt.plot(Y[:,0], Y[:,1],'-k',linewidth=2.0)
	plt.plot(Y[::10,0], Y[::10,1],'*r',label='(.5,.75)')

	#For U,V=(1./16,3./4)
	Y=odeint(Lotka_Volterra,np.array([1./16,3./4]),np.linspace(a,b,subintervals))
	plt.plot(Y[:,0], Y[:,1],'-k',linewidth=2.0)
	plt.plot(Y[::10,0], Y[::10,1],'*g',label='(1./16,3./4)')

	#For U,V=(1./40,3./4)
	Y=odeint(Lotka_Volterra,np.array([1./40,3./4]),np.linspace(a,b,subintervals))
	plt.plot(Y[:,0], Y[:,1],'-k',linewidth=2.0)
	plt.plot(Y[::10,0], Y[::10,1],'*y',label='(1./40,3./4)')

	plt.show()
	#We see that we never reach equilibrium. We circle around the equilibrium points.


def prob4():
	""" Using the RK4 method, approximate the predator-prey trajectories.
	Plot and show the solutions. Create two plots, one for each pair of 
	parameter values alpha, beta. 
	"""
	a, b = 0., 26
	alpha = 1. 
	beta=.3
	dim = 2
	y0 = np.array([1 / 3., 1 / 3.])
	y1=np.array([.5,1/5.])
	def Logistic(y, x):
		return np.array([y[0] * (1. - y[1]-y[0]), alpha * y[1] * (y[0] - beta)])
	
	subintervals=2000
	Y=odeint(Logistic,y0,np.linspace(a,b,subintervals))
	X=odeint(Logistic,y1,np.linspace(a,b,subintervals))
	Y1, Y2 = np.meshgrid(np.arange(0, 2, .1), np.arange(0, 2, .1), sparse=True, copy=False)
	U, V = Logistic((Y1, Y2), 0)
	Q=plt.quiver(Y1[::3,::3],Y2[::3,::3], U[::3,::3], V[::3,::3],pivot='mid',color = 'b',units='dots',width=3.)
	plt.plot(0, 0,'ok',markersize=8)
	plt.axis([-.5, 2, -.5, 2])
	plt.title(" Phase Portrait of the Lotka - Volterra Predator - Prey Model with Logistic Adjustment")
	plt.xlabel('Prey',fontsize=15)
	plt.ylabel('Predators',fontsize=15)

	#Plot solutions
	plt.plot(Y[:,0], Y[:,1],'-r',linewidth=2.0)

	plt.plot(X[:,0], X[:,1],'-y',linewidth=2.0)

	plt.legend()	

	plt.show()

	################Second Graph######################

	a, b = 0., 230
	alpha = 1. 
	beta=1.1
	dim = 2
	y0 = np.array([1 / 3., 1 / 3.])
	y1=np.array([.5,1/5.])
	
	subintervals=2000
	Y=odeint(Logistic,y0,np.linspace(a,b,subintervals))
	X=odeint(Logistic,y1,np.linspace(a,b,subintervals))
	Y1, Y2 = np.meshgrid(np.arange(0, 2, .1), np.arange(0, 2, .1), sparse=True, copy=False)
	U, V = Logistic((Y1, Y2), 0)
	Q=plt.quiver(Y1[::3,::3],Y2[::3,::3], U[::3,::3], V[::3,::3],pivot='mid',color = 'b',units='dots',width=3.)
	plt.plot(0, 0,'ok',markersize=8)
	plt.axis([-.5, 2, -.5, 2])
	plt.title(" Phase Portrait of the Lotka - Volterra Predator - Prey Model with Logistic Adjustment")
	plt.xlabel('Prey',fontsize=15)
	plt.ylabel('Predators',fontsize=15)

	#Plot solutions
	plt.plot(Y[:,0], Y[:,1],'-r',linewidth=2.0)

	plt.plot(X[:,0], X[:,1],'-y',linewidth=2.0)

	plt.legend()	

	plt.show()


	


