from __future__ import division
import numpy as np
from scipy.integrate import odeint
from matplotlib import pyplot as plt


def find_t(f,a,b,alpha,beta,t0,t1,maxI):
	sol1 = 0
	i=0
	while abs(sol1-beta) > 10**-8 and i<maxI:
		sol0 = odeint(f,np.array([alpha,t0]), [a,b],atol=1e-10)[1,0]
		sol1 = odeint(f,np.array([alpha,t1]), [a,b],atol=1e-10)[1,0]
		t2 =  t1 - (sol1 - beta)*(t1-t0)/(sol1-sol0)
		t0 = t1
		t1 = t2
		i=i+1
	if i==maxI:
		print "t not found"
	return t2

def solveSecant(f,X,a,b,alpha,beta,t0,t1,maxI):
	t=find_t(f,a,b,alpha,beta,t0,t1,maxI)
	sol = odeint(f,np.array([alpha,t]), X,atol=1e-10)[:,0]

	return sol

def find_t2(f,a,b,alpha,beta,t0,t1,maxI):
	sol1 = -1
	i=0
	while abs(sol1-beta) > 10**-8 and i<maxI:
		sol0 = odeint(f,np.array([alpha,45,t0]), [a,b],atol=1e-10)[1,0]
		sol1 = odeint(f,np.array([alpha,45,t1]), [a,b],atol=1e-10)[1,0]
		t2 =  t1 - (sol1 - beta)*(t1-t0)/(sol1-sol0)
		t0 = t1
		t1 = t2
		i=i+1
	if i==maxI:
		print "t not found"
	return t2

def solveSecant2(f,X,a,b,alpha,beta,t0,t1,maxI):
	t=find_t2(f,a,b,alpha,beta,t0,t1,maxI)
	sol = odeint(f,np.array([alpha,45,t]), X,atol=1e-10)

	return sol


def prob1():
	def ode(y,x):
		return np.array([y[1], -4*y[0]-9*np.sin(x)])
	X=np.linspace(0,np.pi,100)
	Y1=solveSecant(ode,X,0,np.pi,1,1,0,1,40)
	Y2=solveSecant(ode,X,0,np.pi,1,1,0,-1,40)
	#print "Using t0=0, t1=1 for the first and t0=0, t1=-1 for the second..."
	#print "Initial slopes: 1.0744 and -4.9519"
	plt.plot(X,Y1,'-b',linewidth=2)
	plt.plot(X,Y2,'-r',linewidth=2)
	plt.show()

def prob2():
	def ode(y,x):
		return np.array([y[1],3+2*y[0]/x**2])
	X=np.linspace(1,np.e,100)
	"""Using (np.e**2+6/np.e-6)/(np.e-1) for t0 and 2 for t1"""
	"""finds -5 for the initial slope"""
	Y1=solveSecant(ode,X,1,np.e,6,np.e**2+6/np.e,(np.e**2+6/np.e-6)/(np.e-1),2,40)
	plt.plot(X,Y1,'-k',linewidth=2)
	plt.show()


def prob3(n):
	X=np.linspace(0,195,n+1)
	def ode(y,x):
		mu=.0003
		g=9.8067
		return np.array([np.tan(y[2]),-1*(g*np.sin(y[2])+mu*y[1]**2)/(y[1]*np.cos(y[2])),-1*g/y[1]**2])
	#This section replicates the graph from the lab
	'''
	def ode2(y,x):
		mu=.0000
		g=9.8067
		return np.array([np.tan(y[2]),-1*(g*np.sin(y[2])+mu*y[1]**2)/(y[1]*np.cos(y[2])),-1*g/y[1]**2])
	
	"""Using (np.e**2+6/np.e-6)/(np.e-1) for t0 and 2 for t1"""
	"finds -5 for the initial slope"
	Y1=solveSecant2(ode,X,0,195,0,0,0,-1,400)[:,0]
	Y2=solveSecant2(ode,X,0,195,0,0,0,1,400)[:,0]
	Y3=solveSecant2(ode2,X,0,195,0,0,0,1,400)[:,0]
	Y4=solveSecant2(ode2,X,0,195,0,0,0,-1,400)[:,0]
	plt.plot(X,Y1,'-k',linewidth=2)
	plt.plot(X,Y2,'-k',linewidth=2)
	plt.plot(X,Y3,'-b',linewidth=2)
	plt.plot(X,Y4,'-b',linewidth=2)
	plt.show()
	'''	

	y,v,theta=solveSecant2(ode,X,0,195,0,0,0,-1,400).T

	return  X,(y,v,theta)

