import numpy as np
from matplotlib import pyplot as plt

def prob1(N,a,b):
    """
    Define u(x) = sin( (x + np.pi)**2 - 1).
	return x = np.linspace(a,b,N+1)[1:-1] 
    (the interior points on a grid with N equal subintervals),
	your values approximating .5*u''(x) - u'(x), 
    and the differentiation matrix M = .5*D2 - D1
    """
    def u(x):
        return np.sin( (x + np.pi)**2 - 1)
    X=np.linspace(a,b,N+1)
    h=float(b-a)/N
    secondD=(np.eye(N+1)*-2+np.eye(N+1,k=1)+np.eye(N+1,k=-1))/(h**2)
    firstD=(np.eye(N+1,k=1)-np.eye(N+1,k=-1))/(2*h)
    M=.5*(secondD)-firstD
    U=[u(x) for x in X]
    U=np.dot(M,U)
    return X[1:-1],U[1:-1],M


def prob2(N,epsilon,p3=False):
    """ Numerically approximate the solution of the bvp
    epsilon*u''(x) - u'(x) = f(x), x in (0,1),
    u(0) = 1, u(1) = 3, 
    on the grid x = np.linspace(a,b,N+1).
	
    Plot your solution. 
    """

    X=np.linspace(0,1,N+1)
    h=1./(N)
    D=np.eye(N+1)*(-2*epsilon)+np.eye(N+1,k=1)*(epsilon-(h/2.))+np.eye(N+1,k=-1)*(epsilon+(h/2.))
    D=(1./h**2)*D
    f=[-1 for x in xrange(N+1)]
    f[0],f[-1]=-1-(1.*(epsilon+(h/2.))/h**2),-1-(3.*(epsilon-(h/2.))/h**2)
    R=np.linalg.solve(D,f)	
    if not p3:
    	plt.plot(X, R)
    	plt.show()
    else:
    	return R
      
    
def prob3(N,epsilon):
	""" 
	Use a log-log plot to demonstrate 2nd order convergence of the finite
	difference method used in approximating the solution of the problem 2 in 
	the lab. 
	
	Do not return anything. 
	"""
	N_list = 5*np.array([2**(j+1) for j in range(N)])
	h,max_error=(1.-0)/N_list[:-1],np.ones(N-1)
	
	mesh_best=prob2(N_list[-1],epsilon,p3=True)
	for j in xrange(N-1):
		mesh=prob2(N_list[j],epsilon,p3=True)
		#print mesh
		#print mesh_best[::2**(N-j-1)]
		max_error[j]=np.max(np.abs(mesh-mesh_best[::2**(N-j-1)]))
		
	plt.loglog(h**2,max_error,'.-r',label="$E(h)$")
	plt.loglog(h**2,h,'-k',label="$h^{\, 2}$")
	plt.xlabel("$h$")
	plt.legend(loc='best')
	plt.show()
	#print "The order of the finite difference approximation is about ", ((np.log(max_error[0]) -np.log(max_error[-1]) )/( np.log(h[0]) - np.log(h[-1]) ) ), "."	

#prob3(10,.5)

""" For problems 4-6, you will need to write (& use :) a function 
that solves a general second order linear bvp with Dirichlet 
conditions at both endpoints:

c1(x)u''(x) + c2(x)u'(x) + c3(x)u(x) = f(x), x in (a,b)
u(a) = alpha, u(b) = beta. 

An appropriate function signature would be something like 
the following: 

def fd_order2(N,a,b,alpha,beta,c1,c2,c3,f): 
	
	return np.linspace(a,b,N+1), numerical_approximation

"""
def bvp_solve(N,a,b,alpha,beta,c1,c2,c3,f):
	X=np.linspace(a,b,N+1)
	h=float(b-a)/(N)
	
	D=np.zeros((N+1,N+1))
	
	C3=[c3(y) for y in X]
	C2=[c2(y) for y in X]
	C1=[c1(y) for y in X]
	
	for i in xrange(N+1):
		D[i,i]+=C3[i]-2*C1[i]/h**2

	for i in xrange(N):
		D[i+1,i]+=(C1[i]/h**2)-(C2[i+1]/(2.*h))
		D[i,i+1]+=(C1[i+1]/h**2)+(C2[i]/(2.*h))
		
	D[0,0],D[0,1]=1,0.
	D[-1,-1],D[-1,-2]=1,0.
		
	F=[f(y) for y in X]
	F[0]=alpha
	F[-1]=beta
	
	
	return X,np.linalg.solve(D,F)
	


def prob4(N,epsilon):
	""" Numerically approximate the solution of the problem 4 in the lab.
	
	return x = np.linspace(a,b,N+1), and the approximation u at x
	
	You can check your answer with the following: when N = 10 and epsilon = .1,
	the approximation should be
	
	approximation = 
	[ 0.         -0.0626756  -0.07445629 -0.07518171 -0.07269296 -0.06765727
	 -0.05673975 -0.02697816  0.06334385  0.32759416  1.        ]
	"""
	return bvp_solve(N,0.,np.pi/2,0.,1.,lambda x:epsilon,lambda x:0,lambda x:-4*(np.pi-x**2),lambda x:np.cos(x))
#print prob4(10,.1)

def prob5(N,epsilon):
	""" Numerically approximate the solution of the problem 5 in the lab.
	
	return x = np.linspace(a,b,N+1), and the approximation u at x
	
	You can check your answer with the following: when N = 10 and epsilon = .001,
	the approximation should be
	
	approximation = 
	[-2.         -0.93289014 -1.2830758   0.29230221 -0.13495904  1.13159506
	  2.00336499  0.49595211  0.76087388 -0.66890253  0.        ]
	"""
	return bvp_solve(N,-1.,1.,-2,0.,lambda x:epsilon,lambda x:x,lambda x:0,lambda x:-1*epsilon*(np.pi**2)*np.cos(np.pi*x)-np.pi*x*np.sin(np.pi*x))
#print prob5(10,.1)	
#print prob5(10,.01)
#print prob5(10,.001)

def prob6(N,epsilon):
	""" Numerically approximate the solution of the problem 6 in the lab.
	
	return x = np.linspace(a,b,N+1), and the approximation u at x
	
	You can check your answer with the following: when N = 20 and epsilon = .05,
	the approximation should be 
	
	approximation = 
	[  0.95238095   1.16526611   1.45658263   1.868823     2.47619048
	   3.41543514   4.95238095   7.61904762  12.38095238  19.80952381
	  24.76190476  19.80952381  12.38095238   7.61904762   4.95238095
	   3.41543514   2.47619048   1.868823     1.45658263   1.16526611
	   0.95238095]
	
	
	"""
	return bvp_solve(N,-1.,1.,1./(1.+epsilon),1./(1.+epsilon),lambda x:(epsilon+x**2),lambda x:4*x,lambda x:2,lambda x:0)
#print prob6(20,.02)
#print prob6(20,.05)




