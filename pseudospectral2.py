# Call this lab pseudospectral2.py
from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.fftpack import fft, ifft
from scipy.integrate import odeint

def prob1(N):
	"""
	N = integer
	Define u(x) = np.sin(x)**2.*np.cos(x) + np.exp(2*np.sin(x+1)) .
	Find the Fourier approximation for .5 at the 
	Fourier grid points for a given N.
	Plot the true solution, along with your numerical solution.
	"""
	#N=24
	X=(2.*np.pi/N)*np.arange(1,N+1)
	u=np.sin(X)**2.*np.cos(X) + np.exp(2.*np.sin(X+1))

	k=np.concatenate((np.arange(0,N/2),np.array([0]),np.arange(-N/2+1,0,1)))

	f_hat=fft(u)
	fp_hat=((1j*k)*f_hat)
	fpp_hat=(-k**2)*f_hat
	fp=np.real(ifft(.5*fpp_hat-fp_hat))

	x2 = np.linspace(0,2*np.pi,200)
	numsol = .5*(-2.*np.exp(2*np.sin(x2+1))*np.sin(x2+1)+np.cos(x2)*(2*np.cos(x2)**2-2*np.sin(x2)**2)+4*np.exp(2*np.sin(x2+1))*np.cos(x2+1)**2-5*np.cos(x2)*np.sin(x2)**2)-(2.*np.sin(x2)*np.cos(x2)**2. -np.sin(x2)**3. +2*np.cos(x2+1)*np.exp(2*np.sin(x2+1)))

	plt.plot(X,fp,'*b',label="Solution Using")
	plt.plot(x2,numsol,'-k',label="Numerical Approximation")
	#plt.savefig('spectral2_derivative.pdf')
	plt.show()	

#prob1(24)
def prob2(N=240):
	""" 
	Create the 3d plot of the numerical solution from t=0 to t=8. 
	
	"""
	tsteps=500
	X=np.linspace(0,2*np.pi,N)
	T=np.exp(-100*(X-1)**2)
	C=.2+(np.sin(X-1))**2

	k=np.concatenate((np.arange(0,N/2),np.array([0]),np.arange(-N/2+1,0,1)))

	def G(R,t):
		return np.real(-C*ifft(1j*k*fft(R)))

	Sol=odeint(G,T,np.linspace(0,8,tsteps))

	fig=plt.figure()
	ax=fig.add_subplot('111',projection='3d')
	A,B=np.meshgrid(X,np.linspace(0,8,tsteps))
	ax.plot_surface(A,B,Sol)
	plt.show()

#prob2()
