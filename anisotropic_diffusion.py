import numpy as np
#import numexpr as ne
import time
from scipy.linalg import norm
from scipy.misc import imread, imsave
from matplotlib import pyplot as plt
from matplotlib import cm

# Call your solutions file anisotropic_diffusion.py

# Anisotropic diffusion is a method commonly used to remove static from images. 
# For these problems, you will need to use your own images. Since we will not have much 
# time to fine-tune the algorithm, I recommend using smaller images. 
# 
# For each problem below, I will need you to put 1) the original image, and 2) the 'noisy'
# image in your git repository. Do not put the 'denoised' image in the repository; instead 
# your code should denoise and show the image. 

def add_noise(imagename,changed_pixels=10000):
	# Adds noise to a black and white image
	# Read the image file imagename.
	# Multiply by 1. / 255 to change the values so that they are floating point
	# numbers ranging from 0 to 1.
	IM = imread(imagename, flatten=True) * (1. / 255)
	IM_x, IM_y = IM.shape
	
	for lost in xrange(changed_pixels):
		x_,y_ = random_integers(1,IM_x-2), random_integers(1,IM_y-2)
		val =  .25*randn() + .5
		IM[x_,y_] = max( min(val+ IM[x_,y_],1.), 0.)
	imsave(name=("noised_"+imagename),arr=IM)


U=imread('noisy_bw_pic.png',flatten=True)*(1./255.)

def prob1():
	# Denoise your image, and show your results.
	# Denoise your image, and show your results.

	plt.imshow(U,cmap='gray')
	plt.show()

	#Slow code
	start=time.time()
	Uslow=U
	N=5
	lam=.2
	sigma=.7
	difs = np.empty_like(U)
	g = lambda x: np.exp(-1.*x * x * (1. / sigma**2))
	for i in xrange(N):
		difs[:-1] = g(U[1:] - U[:-1]) * (U[1:] - U[:-1])
		difs[-1] = 0
		difs[1:] += g(U[:-1] - U[1:]) * (U[:-1] - U[1:])
		difs[:,:-1] += g(U[:,1:] - U[:,:-1]) * (U[:,1:] - U[:,:-1])
		difs[:,1:] += g(U[:,:-1] - U[:,1:]) * (U[:,:-1] - U[:,1:])
		difs *= lam
		Uslow += difs
	end=time.time()
	t_original=end-start

	plt.imshow(Uslow,cmap='gray')
	plt.show()
	return t_original
	'''
	#Optimized code
	start=time.time()
	Unew=U.copy()
	U2=U.copy()
	N=50
	lam=.20
	sigma=.7
	
	sinv=np.float32(-1./(sigma**2))
	lam32=np.float32(lam)

	North = U[:-1]
	South = U[1:]
	West = U[:,:-1]
	East = U[:,1:]

	nNorth = Unew[:-1]
	nSouth = Unew[1:]
	nWest = Unew[:,:-1]
	nEast = Unew[:,1:]

	temp = np.empty_like(U)
	vtemp = temp[:-1]
	htemp = temp[:,:-1]

	for i in xrange(N):
		ne.evaluate('lam32*exp(sinv*(South-North)**2)*(South-North)',out=vtemp)
		ne.evaluate('nSouth - vtemp', out=nSouth)
		ne.evaluate('nNorth + vtemp', out=nNorth)
		ne.evaluate('lam32*exp(sinv*(East-West)**2)*(East-West)',out=htemp)
		ne.evaluate('nWest + htemp', out=nWest)
		ne.evaluate('nEast - htemp', out=nEast)
		U2[:]=Unew
	end=time.time()
	t_optimized=end-start
	plt.imshow(U2,cmap='gray')
	plt.show()
	return t_original,t_optimized
	'''

#prob1()

A=imread('noisy_color_pic.png')*(1./255.)

def prob2():
	plt.imshow(A)
	plt.show()
	start=time.time()
	
	Anew=A
	N=5
	lam=.20
	sigma=.7
	difs = np.empty_like(A)
	g = lambda x: np.exp(x * x * (-1. / sigma**2))
	for i in xrange(N):
		difs[:-1] = g(A[1:] - A[:-1]) * (A[1:] - A[:-1])
		difs[-1] = 0
		difs[1:] += g(A[:-1] - A[1:]) * (A[:-1] - A[1:])
		difs[:,:-1] += g(A[:,1:] - A[:,:-1]) * (A[:,1:] - A[:,:-1])
		difs[:,1:] += g(A[:,:-1] - A[:,1:]) * (A[:,:-1] - A[:,1:])
		difs *= lam
		Anew += difs

	end=time.time()

	plt.imshow(Anew)
	plt.show()

	return end-start

#prob2()
'''
def prob3():
	# Denoise your image, and show your results.
	plt.imshow(A)
	plt.show()
	
	start=time.time()

	Anew=A.copy()
	A2=A.copy()
	N=5
	lam=.20
	sigma=.7
	
	sinv=np.float32(-1./(sigma**2))
	lam32=np.float32(lam)

	North = A[:-1]
	South = A[1:]
	West = A[:,:-1]
	East = A[:,1:]

	nNorth = Anew[:-1]
	nSouth = Anew[1:]
	nWest = Anew[:,:-1]
	nEast = Anew[:,1:]

	temp = np.empty_like(A)
	vtemp = temp[:-1]
	htemp = temp[:,:-1]

	sums=np.empty(A.shape[:2], dtype=A.dtype)
	vsums=sums[:-1]
	hsums=sums[:,:-1]
	hsum3D = hsums[...,None]
	vsum3D = vsums[...,None]

	for i in xrange(N):

		ne.evaluate("South-North",out=vtemp)
		ne.evaluate("sum(vtemp**2,axis=2)",out=vsums)
		ne.evaluate("lam32 * exp(sinv * vsum3D) * vtemp",out=vtemp)

		ne.evaluate('nSouth - vtemp', out=nSouth)
		ne.evaluate('nNorth + vtemp', out=nNorth)

		ne.evaluate("East-West",out=htemp)
		ne.evaluate("sum(htemp**2,axis=2)",out=hsums)
		ne.evaluate("lam32 * exp(sinv * hsum3D) * htemp",out=htemp)

		ne.evaluate('nWest + htemp', out=nWest)
		ne.evaluate('nEast - htemp', out=nEast)
		A[:]=Anew
	end=time.time()
	plt.imshow(A)
	plt.show()
	return end-start

print prob2()
print prob3()
'''
