# Call your solutions file waves.py
import numpy as np
from matplotlib import pyplot as plt
import matplotlib.animation as anim

def prob1():
	# For the initial boundary value problem given, plot
	# the true solution and the approximation at t = .5 on the same figure.

	J=5
	M=5
	X=np.linspace(0,1,J+1)
	T=np.linspace(0,.5,M+1)
	h=1./J
	k=.5/M
	lam=k/h
	Y0=[np.sin(2*np.pi*x) for x in X]
	Y1=[(1-((2*np.pi)**2)*(k**2)/2.)*y for y in Y0]
	A=np.eye(J+1)*(2*(1-lam**2))+np.eye(J+1,k=1)*(lam**2)+np.eye(J+1,k=-1)*(lam**2)
	#A[0,0],A[0,1],A[-1,-1],A[-1,-2]=1,0,1,0
	#print A
	Y2=np.dot(A,Y1)-Y0
	Y3=np.dot(A,Y2)-Y1
	Y4=np.dot(A,Y3)-Y2
	Y5=np.dot(A,Y4)-Y3
	Y5[0],Y5[-1]=0,0
	
	plt.plot(X,Y5,'r')
	X2=np.linspace(0,1,100)
	Z=[np.sin(2*np.pi*x)*np.cos(np.pi) for x in X2]
	plt.plot(X2,Z,'k')
	plt.show()
	
	
#prob1()
	
	
	
def prob2():
	# Produce two animations using the given discretizations in space and time. 
	# The second animation should show instability in the numerical solution, due to 
	# the CFL stability condition being broken. 
	
	# You may use either matplotlib or mayavi for the animations.
	J=200
	M=220
	X=np.linspace(0,1,J+1)
	T=np.linspace(0,1,M+1)
	h=1./J
	k=1./M
	lam=k/h
	m=20
	a=m**2
	Y0=[.2*np.exp(-1*(m**2)*(x-.5)**2) for x in X]
	g=[-.4*(m**2)*(x-.5)*np.exp(-1*(m**2)*(x-.5)**2) for x in X]
	f_2prime=[a*.8*((a*(.5-x)**2))*np.exp(-1*a*(x-.5)**2)-a*.4*np.exp(-1*(m**2)*(x-.5)**2) for x in X]
	Y1=np.array(Y0)+k*np.array(g)+((k**2)/2)*np.array(f_2prime)
	A=np.eye(J+1)*(2*(1-lam**2))+np.eye(J+1,k=1)*(lam**2)+np.eye(J+1,k=-1)*(lam**2)
	A[0,0],A[0,1],A[-1,-1],A[-1,-2]=1,0,1,0
	
	Y=[Y0,Y1]
	for x in xrange(M-2):
		Y.append(np.dot(A,Y[-1])-Y[-2])
	
	fig = plt.figure()
	ax = plt.axes(xlim=(0, 1), ylim=(-.3, .3))
	line, = ax.plot([], [], lw=2)
	
	def init():
		line.set_data([], [])
		return line,
	
	def animate(i):
		line.set_data(X, Y[i])
		return line,
	
	an = anim.FuncAnimation(fig, animate, init_func=init, frames=220, interval=20, blit=True)
	
	plt.show()
	
	J=200
	M=180
	X=np.linspace(0,1,J+1)
	T=np.linspace(0,1,M+1)
	h=1./J
	k=1./M
	lam=k/h
	m=20
	a=m**2
	Y0=[.2*np.exp(-1*(m**2)*(x-.5)**2) for x in X]
	g=[-.4*(m**2)*(x-.5)*np.exp(-1*(m**2)*(x-.5)**2) for x in X]
	f_2prime=[a*.8*((a*(.5-x)**2))*np.exp(-1*a*(x-.5)**2)-a*.4*np.exp(-1*(m**2)*(x-.5)**2) for x in X]
	Y1=np.array(Y0)+k*np.array(g)+((k**2)/2)*np.array(f_2prime)
	A=np.eye(J+1)*(2*(1-lam**2))+np.eye(J+1,k=1)*(lam**2)+np.eye(J+1,k=-1)*(lam**2)
	A[0,0],A[0,1],A[-1,-1],A[-1,-2]=1,0,1,0
	
	Y=[Y0,Y1]
	for x in xrange(M-2):
		Y.append(np.dot(A,Y[-1])-Y[-2])
	
	fig = plt.figure()
	ax = plt.axes(xlim=(0, 1), ylim=(-.3, .3))
	line, = ax.plot([], [], lw=2)
	
	def init():
		line.set_data([], [])
		return line,
	
	def animate(i):
		line.set_data(X, Y[i])
		return line,
	
	an = anim.FuncAnimation(fig, animate, init_func=init, frames=180, interval=20, blit=True)
	
	plt.show()

#prob2()	
	
def prob3():
	# Numerically approximate the solution from t = 0 to t = 2 using the given discretization, 
	# and animate your results. 
	
	J=200
	M=440
	X=np.linspace(0,1,J+1)
	T=np.linspace(0,1,M+1)
	h=1./J
	k=1./M
	lam=k/h
	m=20
	Y0=[.2*np.exp(-1*(m**2)*(x-.5)**2) for x in X]
	g=[0 for x in X]
	f_2prime=[.8*(((m**2)*(x-.5))**2)*np.exp(-1*(m**2)*(x-.5)**2)-.2*np.exp(-1*(m**2)*(x-.5)**2) for x in X]
	Y1=np.array(Y0)+k*np.array(g)+((k**2)/2)*np.array(f_2prime)
	A=np.eye(J+1)*(2*(1-lam**2))+np.eye(J+1,k=1)*(lam**2)+np.eye(J+1,k=-1)*(lam**2)
	A[0,0],A[0,1],A[-1,-1],A[-1,-2]=1,0,1,0
	
	Y=[Y0,Y1]
	for x in xrange(M-2):
		Y.append(np.dot(A,Y[-1])-Y[-2])
	
	fig = plt.figure()
	ax = plt.axes(xlim=(0, 1), ylim=(-.3, .3))
	line, = ax.plot([], [], lw=2)
	
	def init():
		line.set_data([], [])
		return line,
	
	def animate(i):
		line.set_data(X, Y[i])
		return line,
	
	an = anim.FuncAnimation(fig, animate, init_func=init, frames=440, interval=20, blit=True)
	
	plt.show()

#prob3()	
	
def prob4():
	# Numerically approximate the solution from t = 0 to t = 2 using the given discretization, 
	# and animate your results.
	
	J=200
	M=440
	X=np.linspace(0,1,J+1)
	T=np.linspace(0,2,M+1)
	h=1./J
	k=2./M
	lam=k/h
	Y0=[(1./3.)*(x<6./11. and x>5./11.) for x in X]
	g=[0 for x in X]
	f_2prime=[0 for x in X]
	for q in xrange(J-1):
		f_2prime[q+1]=Y0[q]-2*Y0[q+1]+Y0[q+2]
	Y1=np.array(Y0)+k*np.array(g)+((lam**2)/2)*np.array(f_2prime)
	A=np.eye(J+1)*(2*(1-lam**2))+np.eye(J+1,k=1)*(lam**2)+np.eye(J+1,k=-1)*(lam**2)
	A[0,0],A[0,1],A[-1,-1],A[-1,-2]=1,0,1,0
	
	Y=[Y0,Y1]
	for x in xrange(M-2):
		Y.append(np.dot(A,Y[-1])-Y[-2])
	
	fig = plt.figure()
	ax = plt.axes(xlim=(0, 1), ylim=(-.3, .3))
	line, = ax.plot([], [], lw=2)
	
	def init():
		line.set_data([], [])
		return line,
	
	def animate(i):
		line.set_data(X, Y[i])
		return line,
	
	an = anim.FuncAnimation(fig, animate, init_func=init, frames=440, interval=20, blit=True)
	
	plt.show()
#prob4()
