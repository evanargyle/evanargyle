# Call this lab pseudospectral1.py

import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import BarycentricInterpolator as BI
from scipy.optimize import fsolve
from mpl_toolkits.mplot3d import Axes3D

def cheb(N):
	x=np.cos((np.pi/N)*np.linspace(0,N,N+1))
	x.shape=(N+1,1)
	lin=np.linspace(0,N,N+1)
	lin.shape = (N+1,1)
	c=np.ones((N+1,1))
	c[0],c[-1]=2.,2.
	c=c*(-1.)**lin
	X=x*np.ones(N+1)

	dX=X-X.T
	D=(c*(1./c).T)/(dX + np.eye(N+1))
	D = D - np.diag(np.sum(D.T,axis=0))
	x.shape = (N+1,)
	return D, x


def prob1():
	"""
	Plot your approximation
	"""
	D,x=cheb(6)
	u=lambda x: np.exp(x)*np.cos(6*x)
	U=[u(g) for g in x]
	a6=D.dot(U)

	b6=BI(x,a6)
	X=np.linspace(-1,1,100)
	B6=[b6(g) for g in X]
	plt.plot(X,B6)
	
	D,x=cheb(8)
	U=[u(g) for g in x]
	a8=D.dot(U)

	b8=BI(x,a8)
	B8=[b8(g) for g in X]
	plt.plot(X,B8,"-")

	D,x=cheb(10)
	U=[u(g) for g in x]
	a10=D.dot(U)

	b10=BI(x,a10)
	B10=[b10(g) for g in X]
	plt.plot(X,B10)

	r=lambda x: np.exp(x)*np.cos(6*x)-6*np.exp(x)*np.sin(6*x)
	R=[r(g) for g in X]
	plt.plot(X,R)

	plt.show()

#prob1()

def prob2():
	""" 
	Plot the true solution, and your numerical solution. Choose a suitable number
	of Chebychev points for the pseudospectral method.
	
	"""
	D,x=cheb(12)
	D=D.dot(D)
	D[0],D[-1]=np.zeros(13),np.zeros(13)
	D[0,0],D[-1,-1]=1.,1.
	print D
	f=[np.exp(2*g) for g in x]
	f[0],f[-1]=0,0
	U=np.linalg.inv(D)
	plt.plot(x,U.dot(f))

	X=np.linspace(-1,1,100)
	R=[(-1*np.cosh(2)-np.sinh(2)*g+np.exp(2*g))*.25 for g in X]
	plt.plot(X,R)

	plt.show()

#prob2()

def prob3():
	""" 
	Plot and show your numerical solution. Choose a suitable number
	of Chebychev points for the pseudospectral method.
	
	"""
	D,x=cheb(12)
	D=D.dot(D)+D
	D[0],D[-1]=np.zeros(13),np.zeros(13)
	D[0,0],D[-1,-1]=1.,1.
	f=[np.exp(3*g) for g in x]
	f[0],f[-1]=-1.,2.
	U=np.linalg.inv(D)
	plt.plot(x,U.dot(f))


	plt.show()

#prob3()

def prob4():
	""" 
	Plot and show your numerical solution. Choose a suitable number
	of Chebychev points for the pseudospectral method.
	
	"""
	D,x=cheb(100)
	S=np.dot(D,D)
	S[0],S[-1]=np.zeros(101),np.zeros(101)
	S[0,0],S[-1,-1]=1.,1.
	def function(x,lam):
		T=np.dot(S,x)-lam*np.sinh(lam*x)
		T[0]=x[0]-1
		T[-1]=0
		return T
	y=np.array([0.]*101)
	y[0],y[-1]=1.,0.
	y2=fsolve(function,y,args=(4))
	y3=fsolve(function,y,args=(8))
	y4=fsolve(function,y,args=(12))
	
	
	plt.plot(x,y2)
	plt.plot(x,y3)
	plt.plot(x,y4)
	plt.show()

#prob4()

def prob5():
	""" 
	Plot and show your numerical solution. On a separate plot, graph the minimizing surface. 
	return chebychev_grid, numerical_solution
	
	"""
	D,x=cheb(30)
	S=np.dot(D,D)
	D[0],D[-1]=np.zeros(31),np.zeros(31)
	D[0,0],D[-1,-1]=1.,1.
	S[0],S[-1]=np.zeros(31),np.zeros(31)
	S[0,0],S[-1,-1]=1.,1.
	y=np.array([7.]*31)
	y[0],y[-1]=7.,1.
	def function(x):
		T=x*np.dot(S,x)-(np.dot(D,x)*np.dot(D,x))-np.ones(x.shape)
		T[0]=x[0]-7
		T[-1]=x[-1]-1
		return T
	y2=fsolve(function,y)

	p=np.linspace(0,2*np.pi,31)
	R, P = np.meshgrid(y2, p)
	print R.shape,P.shape
	#print y2,p,R,P
	X, Y = R*np.cos(P), R*np.sin(P)
	Z = x
	
	fig = plt.figure()
	ax = fig.add_subplot(111, projection='3d')
	ax.plot_surface(Y,X,Z)
	ax.plot_surface(Y,-1*X,Z)
	plt.show()
	
	return x,y2
#prob5()

	
