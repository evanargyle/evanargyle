from __future__ import division
import numpy as np
from matplotlib import pyplot as plt


def initialize_all(y0, t0, t, n):
    """ An initialization routine for the different ODE solving
    methods in the lab. This initializes Y, T, and h. """
    if isinstance(y0, np.ndarray):
        Y = np.empty((n, y0.size)).squeeze()
    else:
        Y = np.empty(n)
    Y[0] = y0
    T = np.linspace(t0, t, n)
    h = float(t - t0) / (n - 1)
    return Y, T, h

def func1(x,y):
    return y-2*x+4

def euler(f, y0, t0, t, n):
    """ Use the Euler method to compute an approximate solution
    to the ODE y' = f(t, y) at n equispaced parameter values from t0 to t
    with initial conditions y(t0) = y0.
    
    y0 is assumed to be either a constant or a one-dimensional numpy array.
    t and t0 are assumed to be constants.
    f is assumed to accept two arguments.
    The first is a constant giving the value of t.
    The second is a one-dimensional numpy array of the same size as y.
    
    This function returns an array Y of shape (n,) if
    y is a constant or an array of size 1.
    It returns an array of shape (n, y.size) otherwise.
    In either case, Y[i] is the approximate value of y at
    the i'th value of np.linspace(t0, t, n).
    """
    y0,t0,h=initialize_all(y0, t0, t, n)
    for x in xrange(len(y0)-1):
        y0[x+1]=y0[x]+h*f(t0[x],y0[x])
    return y0,t0

# optional
def backwards_euler(f, fsolve, y0, t0, t, n):
    """ Use the backward Euler method to compute an approximate solution
    to the ODE y' = f(t, y) at n equispaced parameter values from t0 to t
    with initial conditions y(t0) = y0.
    
    y0 is assumed to be either a constant or a one-dimensional numpy array.
    t and t0 are assumed to be constants.
    f is assumed to accept two arguments.
    The first is a constant giving the value of t.
    The second is a one-dimensional numpy array of the same size as y.
    fsolve is a function that solves the equation
    y(x_{i+1}) = y(x_i) + h f(x_{i+1}, y(x_{i+1}))
    for the appropriate value for y(x_{i+1}).
    It should accept three arguments.
    The first should be the value of y(x_i).
    The second should be the distance between values of t.
    The third should be the value of x_{i+1}.
    
    This function returns an array Y of shape (n,) if
    y is a constant or an array of size 1.
    It returns an array of shape (n, y.size) otherwise.
    In either case, Y[i] is the approximate value of y at
    the i'th value of np.linspace(t0, t, n).
    """
    pass


def midpoint(f, y0, t0, t, n):
    """ Use the midpoint method to compute an approximate solution
    to the ODE y' = f(t, y) at n equispaced parameter values from t0 to t
    with initial conditions y(t0) = y0.
    
    y0 is assumed to be either a constant or a one-dimensional numpy array.
    t and t0 are assumed to be constants.
    f is assumed to accept two arguments.
    The first is a constant giving the value of t.
    The second is a one-dimensional numpy array of the same size as y.
    
    This function returns an array Y of shape (n,) if
    y is a constant or an array of size 1.
    It returns an array of shape (n, y.size) otherwise.
    In either case, Y[i] is the approximate value of y at
    the i'th value of np.linspace(t0, t, n).
    """
    y0,t0,h=initialize_all(y0, t0, t, n)
    for x in xrange(len(y0)-1):
        y0[x+1]=y0[x]+h*f(t0[x]+h/2,y0[x]+(h/2)*f(t0[x],y0[x]))
    return y0,t0


# Optional
def modified_euler(f, y0, t0, t, n):
    """ Use the modified Euler method to compute an approximate solution
    to the ODE y' = f(t, y) at n equispaced parameter values from t0 to t
    with initial conditions y(t0) = y0.
    
    y0 is assumed to be either a constant or a one-dimensional numpy array.
    t and t0 are assumed to be constants.
    f is assumed to accept two arguments.
    The first is a constant giving the value of t.
    The second is a one-dimensional numpy array of the same size as y.
    
    This function returns an array Y of shape (n,) if
    y is a constant or an array of size 1.
    It returns an array of shape (n, y.size) otherwise.
    In either case, Y[i] is the approximate value of y at
    the i'th value of np.linspace(t0, t, n).
    """
    pass


def RK4(f, y0, t0, t, n):
    """ Use the RK4 method to compute an approximate solution
    to the ODE y' = f(t, y) at n equispaced parameter values from t0 to t
    with initial conditions y(t0) = y0.
    
    y0 is assumed to be either a constant or a one-dimensional numpy array.
    t and t0 are assumed to be constants.
    f is assumed to accept two arguments.
    The first is a constant giving the value of t.
    The second is a one-dimensional numpy array of the same size as y.
    
    This function returns an array Y of shape (n,) if
    y is a constant or an array of size 1.
    It returns an array of shape (n, y.size) otherwise.
    In either case, Y[i] is the approximate value of y at
    the i'th value of np.linspace(t0, t, n).
    """
    y0,t0,h=initialize_all(y0, t0, t, n)
    for x in xrange(len(y0)-1):
        K1=f(t0[x],y0[x])
        K2=f(t0[x]+h/2,y0[x]+(h/2)*K1)
        K3=f(t0[x]+h/2,y0[x]+(h/2)*K2)
        K4=f(t0[x+1],y0[x]+h*K3)
        y0[x+1]=y0[x]+(h/6)*(K1+K2*2+K3*2+K4)
    return y0,t0


def prob1():
    """ Test the accuracy of the Euler method using the
    initial value problem y' - y = 4 - 2x, 0 <= x <= 2, y(0) = 0.
    Plot your solutions over the given domain with n as 11, 21, and 41.
    Also plot the exact solution. 
    Show the plot. """
    y1,t1=euler(func1,0,0,2,5)
    y2,t2=euler(func1,0,0,2,10)
    y3,t3=euler(func1,0,0,2,20)
    solx=np.linspace(0,2,1000)
    soly=np.empty(1000)
    for y in xrange(len(soly)):
        soly[y]=solx[y]*2-2+2*np.exp(solx[y])

    plt.plot(solx,soly,label='solution')
    plt.plot(t1,y1,label='h=.4')
    plt.plot(t2,y2,label='h=.2')
    plt.plot(t3,y3,label='h=.1')
    plt.legend()
    plt.show()

def prob2():
    """ Compare the convergence rates of Euler's method, the Midpoint 
	method, and the RK4 method. Use n = 11, 21, 41, 81, and 161 gridpoints.
	Plot the convergence of the methods using a log-log plot, as in 
	Figure 1.3 of the lab. Show the plot.
    """
    Y=2*2-2+2*np.exp(2)
    h=[11,21,41,81,161]
    E=np.empty(5)
    M=np.empty(5)
    K=np.empty(5)
    for i in h:
        ey1,et1=euler(func1,0,0,2,i)
        my1,mt1=midpoint(func1,0,0,2,i)
        ky1,kt1=RK4(func1,0,0,2,i)
        E[h.index(i)]=abs(ey1[-1]-Y)/Y
        M[h.index(i)]=abs(my1[-1]-Y)/Y
        K[h.index(i)]=abs(ky1[-1]-Y)/Y
    
    l=np.array([.2,.1,.05,.025,.0125])
    plt.loglog(l,E,label='Euler h=.1')
    plt.loglog(l,M,label='Midpoint h=.1')
    plt.loglog(l,K,label='RK4 h=.1')
    plt.legend()
    plt.show()

def prob3(n):
    """ Use the RK4 method to solve for the simple harmonic oscillators
	described in the problem.
    Plot and show the solutions. 
	"""
    def func1(y,x):
        return np.array([x[1],x[0]*-1])
    Y1,X1=RK4(func1,np.array([2,-1]),0,20,n)
    
    def func3(y,x):
        return np.array([x[1],x[0]*-1/3])
    Y3,X3=RK4(func3,np.array([2,-1]),0,20,n)
    
    plt.plot(X1,Y1[:,0],label='m=1')
    plt.plot(X3,Y3[:,0],label='m=3')
    plt.legend()
    plt.show()


def prob4(n, gamma):
    """ Use the RK4 method to solve for the damped harmonic oscillator
    problem described in the problem about damped harmonic oscillators.
    Return the array of values at the equispaced points. 
	"""
    def func_1(y,x):
        return np.array([x[1],-1*x[0]-gamma*x[1]])
    def func_2(y,x):
        return np.array([x[1],-1*x[0]-(float(gamma)/2)*x[1]])
    
    Y1,X1=RK4(func_1,np.array([1,-1]),0,20,n)
    Y2,X2=RK4(func_2,np.array([1,-1]),0,20,n)
    '''
    plt.plot(X1,Y1[:,0],label='gamma= '+str(gamma))
    plt.plot(X2,Y2[:,0],label='gamma= '+str(gamma/float(2)))
    plt.legend()
    plt.show()
    '''
    return Y1[:,0]


def prob5(n, gamma, omega):
	""" Use the RK4 method to solve for the damped and forced harmonic 
	oscillator problem.
    Return the array of values at the n equally spaced points. 
	
	Plot and show your solution.
	"""
	def func(x,y):
	    return np.array([y[1],-1*y[0]-gamma*y[1]/2+np.cos(omega*x)])
	
	Y,X=RK4(func,np.array([2,-1]),0,40,n)
	plt.plot(X,Y[:,0])
	plt.show()
	return Y[:,0]
	
prob5(1000,9.,1.)



