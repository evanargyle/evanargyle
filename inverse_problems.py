# Call this lab inverse_problems.py

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import minimize


def prob1(n):
	"""
	n = number of subintervals for the domain [0,1].
	
	Plot your approximation for a(x) at the grid points 
	x = np.linspace(0,1,n+1)
	"""
	def f(x):
		out=-np.ones(x.shape)
		m=np.where(x<.5)
		out[m]=-6*x[m]**2+3.*x[m]-1
		return out
	
	def u(x):
		return (x+1./4)**2+1./4

	def integral_of_f(x):
		out=np.zeros(x.shape)
		s=0
		for i in xrange(1,len(x)):
			if x[i]<=.5:
				s+=(1.5*x[i]**2-2*x[i]**3-x[i])-(1.5*x[i-1]**2-2*x[i-1]**3-x[i-1])
			else:
				s+=(x[i-1]-x[i])
			out[i]=s
		return out

	def derivative_of_u(x):
		out=2*x+.5
		return out

	x = np.linspace(0,1,n+1)
	F, u_p = integral_of_f(x), derivative_of_u(x)
	print F,u_p

	def sum_of_squares(alpha):
		c0=3./8
		return sum((((c0-F)/alpha)-u_p)**2)

	guess=.25*(3-x)
	sol=minimize(sum_of_squares,guess)

	plt.plot(x,sol.x,'-ob',linewidth=2)
	plt.show()
	
#prob1(10)

def prob2(n=10,epsilon=.8):
	"""
	n = number of subintervals for the domain [0,1].
	epsilon is some parameter value > 0.6605
	
	Compute your approximation for a(x) at the grid points 
	x = np.linspace(0,1,n+1)
	
	return x, a(x)
	"""
	def f(x):
		out=-np.ones(x.shape)
		return out
	
	def u(x):
		return x+1+epsilon*np.sin(x/epsilon**2)

	def integral_of_f(x):
		h=1./(n+1)
		out=np.zeros(x.shape)
		for i in xrange(1,len(x)):
			out[i]=out[i-1]-h
		return out

	def derivative_of_u(x):
		out=1+np.cos(x/epsilon**2)/epsilon
		return out

	x = np.linspace(0,1,n+1)
	F, u_p = integral_of_f(x), derivative_of_u(x)

	def sum_of_squares(alpha):
		c0=1
		return sum((((c0-F)/alpha)-u_p)**2)

	guess=.25*(3-x)
	sol=minimize(sum_of_squares,guess)

	plt.plot(x,sol.x,linewidth=2)
	plt.show()

	return x,sol.x

#print prob2(100,.8)
