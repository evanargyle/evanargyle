# Call your solutions file heatflow.py
import numpy as np
from matplotlib import pyplot as plt
import matplotlib.animation as anim

def prob1():
	# For the initial boundary value problem given, plot
	# the initial data and the approximation at t = .4 on the same figure.

	X=np.linspace(0,1,7)
	Y=np.zeros(7)
	h=1./6.
	k=.04
	for x in xrange(7):
		Y[x]=2*max(0,.2-abs(X[x]-.5))
	lam=.05*k/h**2
	D=np.eye(7)*(1-2*lam)+np.eye(7,k=1)*(lam)+np.eye(7,k=-1)*(lam)
	D[0,0],D[0,1],D[-1,-1],D[-1,-2]=1,0,1,0
	
	Y10=Y

	D10=np.linalg.matrix_power(D,10)

	plt.plot(X,Y)
	plt.plot(X,np.dot(D10,Y))
	plt.show()

#prob1()
	
	
def prob2():
	# Produce two animations using the given discretizations in space and time. 
	# The second animation should show instability in the numerical solution, due to 
	# the CFL condition being broken. 
	
	# You may use either matplotlib or mayavi for the animations.
	X=np.linspace(-12,12,141)
	T=np.linspace(0,1,71)
	Y=np.zeros(141)

	h=24./140.
	k=1./70.
	
	for x in xrange(141):
		Y[x]=max(1-X[x]**2,0)

	lam=k/h**2
	D=np.eye(141)*(1-2*lam)+np.eye(141,k=1)*(lam)+np.eye(141,k=-1)*(lam)
	D[0,0],D[0,1],D[-1,-1],D[-1,-2]=1,0,1,0

	fig=plt.figure()

	ax = plt.axes(xlim=(-12, 12), ylim=(-1, 2))
	line, = ax.plot([], [], lw=2)

	results=[Y]
	
	Y1=Y[:]

	for i in xrange(71):
		Y1=np.dot(D,Y1)
		results.append(Y1)

	def animate(i):
		line.set_data(X,results[i])
		return line,

	def init():
		line.set_data([],[])
		return line,

	ani=anim.FuncAnimation(fig,animate,init_func=init,frames=71,interval=71,blit=True)

	plt.show()

	h=24./140.
	k=1./66.

	lam2=k/h**2
	D=np.eye(141)*(1-2*lam2)+np.eye(141,k=1)*(lam2)+np.eye(141,k=-1)*(lam2)
	D[0,0],D[0,1],D[-1,-1],D[-1,-2]=1,0,1,0

	fig2=plt.figure()

	ax2 = plt.axes(xlim=(-12, 12), ylim=(-1, 2))
	line2, = ax2.plot([], [], lw=2)

	T2=np.linspace(0,1,67)

	Y2=Y[:]
	results2=[]

	for i in xrange(67):
		Y2=np.dot(D,Y2)
		results2.append(Y2)

	def animate2(i):
		line2.set_data(X,results[i])
		return line2,

	def init2():
		line2.set_data([],[])
		return line2,

	ani2=anim.FuncAnimation(fig2,animate2,init_func=init2,frames=67,interval=67,blit=True)

	plt.show()
		
#prob2()



def prob3():
	# Reproduce the loglog plot in the lab to demonstrate 2nd order convergence of the 
	# Crank-Nicolson method. 
	steps=[640,20,40,80,160,320]

	error=[]
	h2=[]
	h1=[]

	U640=0

	for x in steps:
		h=24./x
		k=1./x
		X=np.linspace(-12,12,x)
		T=np.linspace(0,1,x)
		Y=np.zeros(x)
		for i in xrange(x):
			Y[i]=max(1-X[i]**2,0)
		lam=k/(2*h**2)
		B=np.eye(x)*(1+2*lam)+np.eye(x)*-1*lam+np.eye(x)*-1*lam
		A=np.eye(x)*(1-2*lam)+np.eye(x)*lam+np.eye(x)*lam
		h2.append(h**2)
		h1.append(h)
		C=np.dot(np.linalg.inv(B),A)
		U=np.linalg.solve(np.linalg.matrix_power(C,x),Y)
		if error==[]:
			error.append(0)
			U640=U
		else:
			s=640/x
			error.append(max(np.abs(U-U640[::s])))
	error.append(error.pop(0))
	h2.append(h2.pop(0))
	h1.append(h1.pop(0))

	plt.loglog(h2,h1,color='b')
	plt.loglog(h2,error,color='r')
	plt.show()

		
#prob3()




