import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import newton

def EmbeddingAlg(param_list, guess, F):
	X=[]
	for param in param_list:
		try:
			x_value = newton(F, guess, fprime=None, args=(param,), tol=1E-7,maxiter=50)
			X.append(x_value)
			guess = x_value
		except RuntimeError:
			return param_list[:len(X)], X

	return param_list, X


def prob1():
	# Using the natural embedding algorithm, produce the plot 
	# requested in exercise 1 in the lab.
	def func(x,lamb):
		return lamb*x-x**3
		
	Y1,X1=EmbeddingAlg(np.linspace(1, -1, 200), -1, func)
	Y2,X2=EmbeddingAlg(np.linspace(1, -1, 200), 1, func)
	Y3,X3=EmbeddingAlg(np.linspace(1, -1, 200), 0, func)
	
	plt.plot(Y1,X1)
	plt.plot(Y2,X2)
	plt.plot(Y3,X3)
	plt.show()
		
#prob1()		
	

def prob2():
	# Using the natural embedding algorithm, produce the plots 
	# requested in exercise 2 in the lab.
	eta=0
	def func(x,lamb):
		return eta+lamb*x-x**3
	ETA=[-1,-.2,.2,1]
	for x in xrange(4):
		eta=ETA[x]
		Y1,X1=EmbeddingAlg(np.linspace(3, -1, 200), -2, func)
		Y2,X2=EmbeddingAlg(np.linspace(3, -1, 200), 2, func)
		Y3,X3=EmbeddingAlg(np.linspace(3, -1, 200), 0, func)
		plt.subplot(4,1,x)
		plt.plot(Y1,X1)
		plt.plot(Y2,X2)
		plt.plot(Y3,X3)
	plt.show()
#prob2()

def prob3():
	# Using the natural embedding algorithm, produce the plot 
	# requested in exercise 3 in the lab.
	r=.56	
	def func(x,k):
		return r*x*(1-x/k)-(x**2)/(1+x**2)
	Y1,X1=EmbeddingAlg(np.linspace(6.45,10, 200), 2, func)
	Y2,X2=EmbeddingAlg(np.linspace(2,10, 200), 2, func)
	Y3,X3=EmbeddingAlg(np.linspace(14,6, 200), 12, func)
	plt.plot(Y1,X1)
	plt.plot(Y2,X2)
	plt.plot(Y3,X3)
	plt.show()
prob3()
	
