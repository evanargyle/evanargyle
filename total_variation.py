import numpy as np
from matplotlib import pyplot as plt
from scipy.misc import imread, imsave
from numpy.random import random_integers, uniform, randn


# Call your solutions file total_variation.py

# Total Variation is a method commonly used to remove static from images. 
# For these problems, you will need to use your own images. Since we will not have much 
# time to fine-tune the algorithm, I recommend using smaller images. 
# 
# For each problem below, I will need you to put 1) the original image, and 2) the 'noisy'
# image in your git repository. Do not put the 'denoised' image in the repository; instead 
# your code should denoise and show the image. 

# The code below can be used to add noise to your images. 


def add_noise(imagename,changed_pixels=50000):
	# Adds noise to a black and white image
	# Read the image file imagename.
	# Multiply by 1. / 255 to change the values so that they are floating point
	# numbers ranging from 0 to 1.
	IM = imread(imagename, flatten=True) * (1. / 255)
	IM_x, IM_y = IM.shape
	
	for lost in xrange(changed_pixels):
		x_,y_ = random_integers(1,IM_x-2), random_integers(1,IM_y-2)
		val =  .25*randn() + .5
		IM[x_,y_] = max( min(val+ IM[x_,y_],1.), 0.)
	imsave(name=("noised_"+imagename),arr=IM)

def prob1():
	# Denoise your (black and white) image.
	# Show the noisy image, and the denoised images. 
	U=imread('noised_pic.png')*(1./255)
	dt=1e-3
	lam=40
	dx=1
	dy=1
	nx=len(U)
	ny=len(U[0])
	F=np.copy(U)

	def rhs(U):
		Uxx=np.roll(U,-1,axis=0) - 2*U + np.roll(U,1,axis=0)
		Uyy=np.roll(U,-1,axis=1) - 2*U + np.roll(U,1,axis=1)
		U-=dt*(U-F-lam*(Uxx+Uyy))

	for n in xrange(250):
		print "\r"+str(n),
		rhs(U)

	plt.subplot(1,2,1)
	plt.imshow(F,cmap='gray')
	plt.subplot(1,2,2)
	plt.imshow(U,cmap='gray')
	plt.show()
	
#prob1()

def prob2():
	# Denoise your (black and white) image.
	# Show the noisy image, and the denoised images. 
	U=imread('noised_pic.png')*(1./255)
	dt=1e-3
	lam=1
	dx=1
	dy=1
	nx=len(U)
	ny=len(U[0])
	F=np.copy(U)

	def rhs(U):
		Uxx=np.roll(U,-1,axis=0) - 2*U + np.roll(U,1,axis=0)
		Uyy=np.roll(U,-1,axis=1) - 2*U + np.roll(U,1,axis=1)
		Ux=(np.roll(U,-1,axis=0) - np.roll(U,1,axis=0))/2.
		Uy=(np.roll(U,-1,axis=1) - np.roll(U,1,axis=1))/2.
		Uxy=(np.roll(Ux,-1,axis=1) - np.roll(Ux,1,axis=1))/2.
		U+=dt*(-lam*(U-F)+(Uxx*Uy**2+Uyy*Ux**2-2*Ux*Uy*Uxy)/(1e-6+Ux**2+Uy**2)**1.5)

	for n in xrange(200):
		print "\r"+str(n),
		rhs(U)

	plt.subplot(1,2,1)
	plt.imshow(F,cmap='gray')
	plt.subplot(1,2,2)
	plt.imshow(U,cmap='gray')
	plt.show()

#prob2()









