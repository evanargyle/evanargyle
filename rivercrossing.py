import numpy as np
from matplotlib import pyplot as plt
from scipy.optimize import fsolve
from scipy.misc import derivative as der

def prob1(y,x):
	def c(x):
		return -.7*(x-1)*(x+1)

	def alpha(x):
		return 1./np.sqrt(1-c(x)**2)

	def beta(x):
		return (alpha(x)*der(y,x))**2

	def gamma(x):
		return c(x)*der(y,x)*(alpha(x)**2)

	def L(x):
		return alpha(x)*np.sqrt(1+beta(x))-gamma(x)

	return L(x)

	
def prob2(y=lambda x: 2.5*x+2.5,num=10000):
	X=np.linspace(-1,1,num+1)
	step=2./num	
	s=0
	for x in xrange(num):
		s+=step*prob1(y,X[x])		
	return (2,s)


def cheb(N):
	x=np.cos((np.pi/N)*np.linspace(0,N,N+1))
	x.shape=(N+1,1)
	lin=np.linspace(0,N,N+1)
	lin.shape = (N+1,1)
	c=np.ones((N+1,1))
	c[0],c[-1]=2.,2.
	c=c*(-1.)**lin
	X=x*np.ones(N+1)

	dX=X-X.T
	D=(c*(1./c).T)/(dX + np.eye(N+1))
	D = D - np.diag(np.sum(D.T,axis=0))
	x.shape = (N+1,)
	return D, x

def prob3():
	N=100
	D,X=cheb(N)
	
	def c(x):
		return -.7*(x-1)*(x+1)

	def alpha(x):
		return 1./np.sqrt(1-c(x)**2)

	def DL(y,yp):
		return [((1+(alpha(X[x])**2*yp[x]**2))**-.5)*yp[x]*alpha(X[x])**3-(alpha(X[x])**2)*c(X[x]) for x in xrange(len(yp))]

	def g(z):
		out=D.dot(DL(z,D.dot(z)))
		out[0],out[-1]=z[0]-5,z[-1]
		return out

	guess=2.5*X+2.5

	R=fsolve(g,guess)

	plt.plot(X,R)
	plt.plot(X,2.5*X+2.5)
	plt.show()


def prob4():
	N=100
	D,X=cheb(N)
	
	def c(x):
		return -.7*(x-1)*(x+1)

	def alpha(x):
		return 1./np.sqrt(1-c(x)**2)

	def DL(y,yp):
		return [((1+(alpha(X[x])**2*yp[x]**2))**-.5)*yp[x]*alpha(X[x])**3-(alpha(X[x])**2)*c(X[x]) for x in xrange(len(yp))]

	def g(z):
		out=D.dot(DL(z,D.dot(z)))
		out[0],out[-1]=z[0]-5,z[-1]
		return out

	guess=2.5*X+2.5

	R=fsolve(g,guess)
	DY=D.dot(R)

	def theta(t,x):
		return ((np.sin(t)+C[x])/np.cos(t))-DY[x]
	
	C=[c(x) for x in X]
	T=[fsolve(theta,np.pi/4,args=n) for n in xrange(N+1)]
	plt.plot(X,T)
	plt.yticks([0,np.pi/6,np.pi/3,np.pi/2],["0","pi/6","pi/3","pi/2"])
	plt.show()




