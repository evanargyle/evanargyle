import numpy as np
from mayavi import mlab
from scipy.integrate import odeint
from matplotlib import pyplot as plt


def prob1():
	a=10.
	b=8./3.
	c=28.
	def func(y,x):
		return [a*(y[1]-y[0]),c*y[0]-y[1]-y[0]*y[2],y[0]*y[1]-b*y[2]]
	for q in xrange(15):
		g,h,i=np.random.randint(-15,15),np.random.randint(-15,15),np.random.randint(-15,15)
		res=odeint(func,np.array([g,h,i]),np.linspace(0,10,1000))
		mlab.plot3d(res[:,0],res[:,1],res[:,2],color=(.1,q/15.,.1),tube_radius=.25)
	mlab.show()
#prob1()

def prob2(m,T,res,step,delay):
	# Produce the plot requested in the lab.
	# 
	# m = number of trajectories to plot,
	# T = final time value
	# res = resolution for the plot
	# step = stepping number = number of new points to include
	# at each update of the plot
	# delay = time delay to use between iterations
	
	
	a=10.
	b=8./3.
	c=28.
	t=np.linspace(0,T,res)
	x=np.zeros((m,t.size))
	y=np.zeros((m,t.size))
	z=np.zeros((m,t.size))
	lines=[]
	def func(u,p):
		return [a*(u[1]-u[0]),c*u[0]-u[1]-u[0]*u[2],u[0]*u[1]-b*u[2]]
	for q in xrange(m):
		g,h,i=np.random.randint(-15,15),np.random.randint(-15,15),np.random.randint(-15,15)
		res=odeint(func,np.array([g,h,i]),t)
		x[q]=res[:,0]
		y[q]=res[:,1]
		z[q]=res[:,2]
	for q in xrange(m):
		lines.append(mlab.plot3d(x[q][:1],y[q][:1],res[q][:1],tube_radius=.25,color=(0.1,q/float(m),0.1)))
	@mlab.show
	@mlab.animate(delay=delay)
	def animate():
		scale=0
		for i in xrange(2+step,t.size,step):
			for q in xrange(len(lines)):
				lines[q].mlab_source.reset(x=x[q,:i],y=y[q,:i],z=z[q,:i])
			mlab.gcf().scene.reset_zoom()
			yield
	animate()
#prob2(10,100,10000,10,50)
	
def prob3(T,res,step,delay):
	# Produce the plot requested in the lab.
	# 
	# T, res, step, and delay are defined as in prob2
	
	a=10.
	b=8./3.
	c=28.
	m=2
	lines=[]
	t=np.linspace(0,T,res)
	x=np.zeros((m,t.size))
	y=np.zeros((m,t.size))
	z=np.zeros((m,t.size))
	
	def func(u,p):
		return [a*(u[1]-u[0]),c*u[0]-u[1]-u[0]*u[2],u[0]*u[1]-b*u[2]]
	g,h,i=np.random.randint(-15,15),np.random.randint(-15,15),np.random.randint(-15,15)
	res=odeint(func,np.array([g,h,i]),t,atol=1E-14,rtol=1E-12)
	x[0]=res[:,0]
	y[0]=res[:,1]
	z[0]=res[:,2]
	res=odeint(func,np.array([g,h,i]),t,atol=1E-15,rtol=1E-13)
	x[1]=res[:,0]
	y[1]=res[:,1]
	z[1]=res[:,2]
	for q in xrange(m):
		lines.append(mlab.plot3d(x[q][:1],y[q][:1],res[q][:1],tube_radius=.25,color=(0.1,q/float(m),0.1)))
	@mlab.show
	@mlab.animate(delay=delay)
	def animate():
		scale=0
		for i in xrange(2+step,t.size,step):
			for q in xrange(len(lines)):
				lines[q].mlab_source.reset(x=x[q,:i],y=y[q,:i],z=z[q,:i])
			mlab.gcf().scene.reset_zoom()
			yield
	animate()

#prob3(100,100000,100,50)

def prob4(T,res,step,delay):
	# Produce the plot requested in the lab.
	# 
	# T, res, step, and delay are defined as in prob2
	#
	# Make sure that the ode solver uses stringent values for 
	# the absolute and relative errors: atol= 1e-15, rtol=1e-13
	a=10.
	b=8./3.
	c=28.
	m=2
	lines=[]
	t=np.linspace(0,T,res)
	x=np.zeros((m,t.size))
	y=np.zeros((m,t.size))
	z=np.zeros((m,t.size))
	
	def func(u,p):
		return [a*(u[1]-u[0]),c*u[0]-u[1]-u[0]*u[2],u[0]*u[1]-b*u[2]]
	g,h,i=np.random.randint(-15,15),np.random.randint(-15,15),np.random.randint(-15,15)
	res=odeint(func,np.array([g,h,i]),t)
	x[0]=res[:,0]
	y[0]=res[:,1]
	z[0]=res[:,2]
	g*=(1.+2.22*10**-16)
	res=odeint(func,np.array([g,h,i]),t)
	x[1]=res[:,0]
	y[1]=res[:,1]
	z[1]=res[:,2]
	for q in xrange(m):
		lines.append(mlab.plot3d(x[q][:1],y[q][:1],res[q][:1],tube_radius=.25,color=(0.1,q/float(m),0.1)))
	@mlab.show
	@mlab.animate(delay=delay)
	def animate():
		scale=0
		for i in xrange(2+step,t.size,step):
			for q in xrange(len(lines)):
				lines[q].mlab_source.reset(x=x[q,:i],y=y[q,:i],z=z[q,:i])
			mlab.gcf().scene.reset_zoom()
			yield
	animate()

#prob4(100,10000,10,50)


def prob5():
	# Produce the plot requested in the lab.
	# 
	# calculate and return your approximation for the Lyapunov 
	# exponent
	
	a=10.
	b=8./3.
	c=28.
	t=np.linspace(0,9,1000)
	#g,h,i=np.random.randint(-15,15),np.random.randint(-15,15),np.random.randint(-15,15)
	g,h,i=7,6,6
	def func(u,p):
		return [a*(u[1]-u[0]),c*u[0]-u[1]-u[0]*u[2],u[0]*u[1]-b*u[2]]
	res=odeint(func,np.array([g,h,i]),t,atol=1E-15,rtol=1E-13)
	res2=odeint(func,np.array([g*(1+2.22*10**-16),h,i]),t,atol=1E-15,rtol=1E-13)
	difx=abs(res[:,0]-res2[:,0])
	dify=abs(res[:,1]-res2[:,1])
	difz=abs(res[:,2]-res2[:,2])
	summ=difx+dify+difz
	l=[np.log(x) for x in summ]
	exp=np.polyfit(t,l,1)
	print g,h,i
	print exp
	line=[np.exp(y*exp[0]+exp[1]) for y in t]
	plt.semilogy(t,summ)
	plt.semilogy(t,line)
	plt.show()
	
	return exp[0]
#prob5()
	
	